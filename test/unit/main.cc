// Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md.
// Licensed under the BSD 3-Clause license. See LICENSE.md.

#include "gtest/gtest.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
