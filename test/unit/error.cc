// Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md.
// Licensed under the BSD 3-Clause license. See LICENSE.md.

#include "error.hh"

#include "gtest/gtest.h"

struct ExceptionFixture : ::testing::Test
{
 protected:
  std::string what = "message";
  using t_type = sekreta::Exception::kType;
};

TEST_F(ExceptionFixture, RuntimeError)
{
  ASSERT_THROW(
      {
        try
          {
            throw sekreta::exception::RuntimeError(what);
          }
        catch (const sekreta::Exception& ex)
          {
            ASSERT_EQ(ex.type(), t_type::RuntimeError);
            ASSERT_EQ(ex.what(), what);
            throw;
          }
      },
      sekreta::Exception);
}

TEST_F(ExceptionFixture, RuntimeError_CopyAssignment)
{
  sekreta::exception::RuntimeError one(what);
  sekreta::exception::RuntimeError two;

  two = one;

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, RuntimeError_CopyCtor)
{
  sekreta::exception::RuntimeError one(what);
  sekreta::exception::RuntimeError two(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, RuntimeError_MoveAssignment)
{
  sekreta::exception::RuntimeError one(what);
  sekreta::exception::RuntimeError two;

  two = std::move(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, RuntimeError_MoveCtor)
{
  sekreta::exception::RuntimeError one(what);
  sekreta::exception::RuntimeError two(std::move(one));

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InvalidArgument)
{
  ASSERT_THROW(
      {
        try
          {
            throw sekreta::exception::InvalidArgument(what);
          }
        catch (const sekreta::Exception& ex)
          {
            ASSERT_EQ(ex.type(), t_type::InvalidArgument);
            ASSERT_EQ(ex.what(), what);
            throw;
          }
      },
      sekreta::Exception);
}

TEST_F(ExceptionFixture, InvalidArgument_CopyAssignment)
{
  sekreta::exception::InvalidArgument one(what);
  sekreta::exception::InvalidArgument two;

  two = one;

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InvalidArgument_CopyCtor)
{
  sekreta::exception::InvalidArgument one(what);
  sekreta::exception::InvalidArgument two(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InvalidArgument_MoveAssignment)
{
  sekreta::exception::InvalidArgument one(what);
  sekreta::exception::InvalidArgument two;

  two = std::move(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InvalidArgument_MoveCtor)
{
  sekreta::exception::InvalidArgument one(what);
  sekreta::exception::InvalidArgument two(std::move(one));

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InternalError)
{
  ASSERT_THROW(
      {
        try
          {
            throw sekreta::exception::InternalError(what);
          }
        catch (const sekreta::Exception& ex)
          {
            ASSERT_EQ(ex.type(), t_type::InternalError);
            ASSERT_EQ(ex.what(), what);
            throw;
          }
      },
      sekreta::Exception);
}

TEST_F(ExceptionFixture, InternalError_CopyAssignment)
{
  sekreta::exception::InternalError one(what);
  sekreta::exception::InternalError two;

  two = one;

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InternalError_CopyCtor)
{
  sekreta::exception::InternalError one(what);
  sekreta::exception::InternalError two(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InternalError_MoveAssignment)
{
  sekreta::exception::InternalError one(what);
  sekreta::exception::InternalError two;

  two = std::move(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, InternalError_MoveCtor)
{
  sekreta::exception::InternalError one(what);
  sekreta::exception::InternalError two(std::move(one));

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, NotImplemented)
{
  ASSERT_THROW(
      {
        try
          {
            throw sekreta::exception::NotImplemented(what);
          }
        catch (const sekreta::Exception& ex)
          {
            ASSERT_EQ(ex.type(), t_type::NotImplemented);
            ASSERT_EQ(ex.what(), what);
            throw;
          }
      },
      sekreta::Exception);
}

TEST_F(ExceptionFixture, NotImplemented_CopyAssignment)
{
  sekreta::exception::NotImplemented one(what);
  sekreta::exception::NotImplemented two;

  two = one;

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, NotImplemented_CopyCtor)
{
  sekreta::exception::NotImplemented one(what);
  sekreta::exception::NotImplemented two(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, NotImplemented_MoveAssignment)
{
  sekreta::exception::NotImplemented one(what);
  sekreta::exception::NotImplemented two;

  two = std::move(one);

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, NotImplemented_MoveCtor)
{
  sekreta::exception::NotImplemented one(what);
  sekreta::exception::NotImplemented two(std::move(one));

  ASSERT_EQ(one.type(), two.type());
  ASSERT_EQ(one.what(), two.what());
}

TEST_F(ExceptionFixture, SEK_THROW)
{
  try
    {
      SEK_THROW(sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      ASSERT_EQ(ex.type(), t_type::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_IF)
{
  try
    {
      SEK_THROW_IF(false, sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      ASSERT_EQ(ex.type(), t_type::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_ASSERT)
{
  try
    {
      SEK_THROW_ASSERT(sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      ASSERT_EQ(ex.type(), t_type::RuntimeError);
    }
}

TEST_F(ExceptionFixture, SEK_THROW_ASSERT_IF)
{
  try
    {
      SEK_THROW_ASSERT_IF(false, sekreta::exception::RuntimeError, message);
    }
  catch (const sekreta::Exception& ex)
    {
      ASSERT_EQ(ex.type(), t_type::RuntimeError);
    }
}
