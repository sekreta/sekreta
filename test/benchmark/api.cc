// Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md.
// Licensed under the BSD 3-Clause license. See LICENSE.md.

#include "api.hh"

#include <benchmark/benchmark.h>

namespace api = sekreta::api;
namespace type = sekreta::type;

class API : public benchmark::Fixture
{
 public:
  using t_uid = type::UID<std::string_view, uint32_t>;
  using t_one = api::kovri::Library;
  using t_two = api::kovri::Socket;

  api::Factory<t_uid> factory;
  t_uid uid{{"uid"}, {}};
};

// TODO(anonimal): timings commented out until this is resolved:
//   https://github.com/google/benchmark/issues/797

BENCHMARK_F(API, CheckIn_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.check_in<t_one>(uid);
}

BENCHMARK_F(API, CheckIn_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
    }
}

BENCHMARK_F(API, CheckIn_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      uid.num(uid.num() + 1);
      // state.ResumeTiming();
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
    }
}

BENCHMARK_F(API, CheckOut_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      // state.ResumeTiming();
      factory.check_out<t_one>(uid);
    }
}

BENCHMARK_F(API, CheckOut_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.check_out<t_one>(uid);
      factory.check_out<t_two>(uid);
    }
}

BENCHMARK_F(API, CheckOut_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      uid.num(uid.num() + 1);
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.check_out<t_one>(uid);
      factory.check_out<t_two>(uid);
    }
}

BENCHMARK_F(API, Create_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.create<t_one>(uid);
}

BENCHMARK_F(API, Create_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    factory.create<t_one>(uid);
}

BENCHMARK_F(API, Create_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      uid.num(uid.num() + 1);
      // state.ResumeTiming();
      factory.create<t_one>(uid);
      factory.create<t_two>(uid);
    }
}

BENCHMARK_F(API, Borrow_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      // state.ResumeTiming();
      factory.borrow<t_one>(uid);
    }
}

BENCHMARK_F(API, Borrow_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.borrow<t_one>(uid);
      factory.borrow<t_two>(uid);
    }
}

BENCHMARK_F(API, Borrow_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      uid.num(uid.num() + 1);
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.borrow<t_one>(uid);
      factory.borrow<t_two>(uid);
    }
}

BENCHMARK_F(API, Find)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      // state.ResumeTiming();
      factory.find<t_one>(uid);
    }
}

BENCHMARK_F(API, View)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      // state.ResumeTiming();
      factory.view();
    }
}

BENCHMARK_F(API, Erase_Single)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      // state.ResumeTiming();
      factory.erase<t_one>(uid);
    }
}

BENCHMARK_F(API, Erase_Multiple_SameKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.erase<t_one>(uid);
      factory.erase<t_two>(uid);
    }
}

BENCHMARK_F(API, Erase_Multiple_MultipleKey)
(benchmark::State& state)
{
  for (auto _ : state)
    {
      // state.PauseTiming();
      uid.num(uid.num() + 1);
      factory.check_in<t_one>(uid);
      factory.check_in<t_two>(uid);
      // state.ResumeTiming();
      factory.erase<t_one>(uid);
      factory.erase<t_two>(uid);
    }
}

// TODO(anonimal): more benchmarks
