| Author   | Contact |
| -------- | ------- |
| anonimal | <anonimal@sekreta.org> - <anonimal@kovri.io> - <anonimal@getmonero.org> |

Use the `git log --show-signature -p` command to see individuals (and their respective work) who may not be listed in this file.
