//! \file
//!
//! \brief SWIG interface file to Sekreta's error handling
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details

%module pysek_error
%{ #include "error.hh" %}

%include "error.hh"

//! \todo SWIG-friendly exception handling
//%include "exception.i"

//! \todo overloading + std::exception (see build warnings)
