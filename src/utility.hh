//! \file utility.hh
//!
//! \brief All utility code, used internally and externally
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details
//!
//! \details Filesystem related, network address parsing, serialization, etc.
//!   are all welcome in this file
//!
//! \todo Merge WIP branch when in working condition

#ifndef SRC_UTILITY_HH_
#define SRC_UTILITY_HH_

#endif  // SRC_UTILITY_HH_
