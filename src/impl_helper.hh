//! \file impl_helper.hh
//!
//! \brief Optional super-project implementation helper
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details
//!
//! \details Contains reusable common idioms that can be used across multiple
//!   super-project implementations
//!
//! \note This file is *not* a requirement for Sekreta usage, neither internally
//!   nor externally

#ifndef SRC_IMPL_HELPER_HH_
#define SRC_IMPL_HELPER_HH_

#include <array>
#include <optional>
#include <utility>
#include <vector>

#include "api.hh"
#include "type.hh"

namespace sekreta::api::impl_helper
{
//! \brief Types to be used by both super-project and Sekreta
namespace type
{
//! \brief Location of Sekreta instance
enum struct kLocation : uint8_t
{
  Client,
  Daemon,
  Wallet,  //! Cryptocurrency-specific
};

//! \brief Command given to underlying anonymity system
enum struct kCommand : uint8_t
{
  Configure,
  Start,
  Stop,
  Restart,
  Status,
};

//! \brief Implementation errors
enum struct kError : uint8_t
{
  Unsupported,
  Count,
  Location,
  Command,
  System,
  Address,
  Network,
};

//! \brief Public alias for all supported anonymity systems
//! \note Guaranteed to be the same type that it's aliased to
using kSystem = ::sekreta::type::kSystem;

//! \brief Public argument alias type
//! \details Guarantees best-practic trivial destruction for static duration
//! \note This guarantee requires though that the given types must
//!   *also* be trivially destructible
template <typename t_key, typename t_value, size_t t_size>
using Args = std::array<std::pair<t_key, t_value>, t_size>;

}  // namespace type

//! \brief Generic argument implementation
template <typename t_value = std::string_view>
class Args
{
 public:
  Args() = default;
  virtual ~Args() = default;

  //! \brief Default copy-ctor
  Args(const Args&) = default;

  //! \brief Default copy-assignment
  Args& operator=(const Args&) = default;

  //! \brief Default move-ctor
  Args(Args&&) = default;

  //! \brief Default move-assignment
  Args& operator=(Args&&) = default;

 public:
  //! \brief Argument key accessor
  template <typename t_key>
  static std::optional<t_key> get_key(const t_value& value)
  {
    if constexpr (std::is_same_v<t_key, type::kLocation>)
      {
        return process_key<t_key, t_location>(m_location, value);
      }
    else if constexpr (std::is_same_v<t_key, type::kCommand>)
      {
        return process_key<t_key, t_command>(m_command, value);
      }
    else if constexpr (std::is_same_v<t_key, type::kSystem>)
      {
        return process_key<t_key, t_system>(m_system, value);
      }
    else if constexpr (std::is_same_v<t_key, type::kError>)
      {
        return process_key<t_key, t_error>(m_error, value);
      }

    return std::nullopt;
  }

  //! \brief Argument value accessor
  template <typename t_key>
  static std::optional<t_value> get_value(const t_key& key)
  {
    if constexpr (std::is_same_v<t_key, type::kLocation>)
      {
        return process_value<t_key, t_location>(m_location, key);
      }
    else if constexpr (std::is_same_v<t_key, type::kCommand>)
      {
        return process_value<t_key, t_command>(m_command, key);
      }
    else if constexpr (std::is_same_v<t_key, type::kSystem>)
      {
        return process_value<t_key, t_system>(m_system, key);
      }
    else if constexpr (std::is_same_v<t_key, type::kError>)
      {
        return process_value<t_key, t_error>(m_error, key);
      }

    return std::nullopt;
  }

 protected:
  //! \brief Argument key processor
  template <typename t_key, typename t_arg>
  static std::optional<t_key> process_key(
      const t_arg& arg,
      const t_value& value)
  {
    for (const auto& a : arg)
      {
        if (a.second == value)
          return a.first;
      }

    return std::nullopt;
  }

  //! \brief Argument value processor
  template <typename t_key, typename t_arg>
  static std::optional<t_value> process_value(
      const t_arg& arg,
      const t_key& key)
  {
    for (const auto& a : arg)
      {
        if (a.first == key)
          return a.second;
      }

    return std::nullopt;
  }

 private:
  static constexpr size_t location_size{3};
  //! \brief Internal alias for location-type
  using t_location = type::Args<type::kLocation, t_value, location_size>;
  static const t_location m_location;

  static constexpr size_t command_size{5};
  //! \brief Internal alias for command-type
  using t_command = type::Args<type::kCommand, t_value, command_size>;
  static const t_command m_command;

  static constexpr size_t system_size{5};
  //! \brief Internal alias for system-type
  using t_system = type::Args<type::kSystem, t_value, system_size>;
  static const t_system m_system;

  static constexpr size_t error_size{7};
  //! \brief Internal alias for error-type
  using t_error = type::Args<type::kError, t_value, error_size>;
  static const t_error m_error;
};

template <typename t_value>
const type::Args<type::kLocation, t_value, Args<t_value>::location_size>
    Args<t_value>::m_location{{
        {type::kLocation::Client, "client"},
        {type::kLocation::Daemon, "daemon"},
        {type::kLocation::Wallet, "wallet"},
    }};

template <typename t_value>
const type::Args<type::kCommand, t_value, Args<t_value>::command_size>
    Args<t_value>::m_command{{
        {type::kCommand::Configure, "configure"},
        {type::kCommand::Restart, "restart"},
        {type::kCommand::Start, "start"},
        {type::kCommand::Status, "status"},
        {type::kCommand::Stop, "stop"},
    }};

template <typename t_value>
const type::Args<type::kSystem, t_value, Args<t_value>::system_size>
    Args<t_value>::m_system{{
        {type::kSystem::I2P, "i2p"},
        {type::kSystem::Ire, "ire"},
        {type::kSystem::Kovri, "kovri"},
        {type::kSystem::Loki, "loki"},
        {type::kSystem::Tor, "tor"},
    }};

template <typename t_value>
const type::Args<type::kError, t_value, Args<t_value>::error_size>
    Args<t_value>::m_error{{
        {type::kError::Address, "Invalid address"},
        {type::kError::Command, "Invalid command"},
        {type::kError::Count, "Invalid count of arguments"},
        {type::kError::Location, "Invalid location"},
        {type::kError::Network, "Invalid network or network not supported"},
        {type::kError::System, "Invalid system or system not supported"},
        {type::kError::Unsupported, "Unsupported argument"},
    }};

//! \brief Daemon arguments data structure
template <typename t_value>  // TODO(unassigned): t_container
struct ArgsData
{
 protected:
  ~ArgsData() = default;

 public:
  t_value command;  //!< Command type argument
  t_value system;  //!< System type argument
  std::vector<t_value> system_args;  //!< Arguments to system type argument
};

//! \brief Daemon arguments data structure
//! \tparam t_value Arguments value type
template <typename t_value>
#ifdef SWIG
struct ServerArgs : public Args<t_value>, ArgsData<t_value>
#else
struct ServerArgs final : public Args<t_value>, ArgsData<t_value>
#endif
{};

//! \brief Wallet arguments data structure
//! \tparam t_value Arguments value type
template <typename t_value>
#ifdef SWIG
struct ClientArgs : public Args<t_value>, ArgsData<t_value>
#else
struct ClientArgs final : public Args<t_value>, ArgsData<t_value>
#endif
{
  //! \brief Whether Sekreta runs within superproject's client or daemon
  t_value location;
};

//! \brief Alias for a server-based daemon
//! \note Guaranteed to be the same type that it's aliased to
template <typename t_value>
using DaemonArgs = ServerArgs<t_value>;

//! \brief Alias for a client-based wallet
//! \note Guaranteed to be the same type that it's aliased to
template <typename t_value>
using WalletArgs = ClientArgs<t_value>;

//! \brief Argument error code handler
//! \tparam t_args Arguments data structure type
//! \tparam t_value Error value (message) type
template <typename t_args, typename t_value = std::string_view>
std::pair<std::optional<t_args>, std::optional<t_value>> args_error(
    const type::kError code)
{
  return {std::nullopt, t_args::template get_value<type::kError>(code)};
}

//!
//! \brief Generic argument parser for both superprojects with their own
//!   client/daemon
//!
//! \details Offers extensibility and future-proofing for arg-caller
//!   implementations by setting a standard of callability. Allows running of
//!   seperate instances within a superproject's client and/or daemon.
//!
//! \tparam t_args Arguments data structure type
//! \tparam t_value Arguments value type
//!
//! \return Pair of optionals:
//!   first = parsed args if successful (empty if not)
//!   second = error message if applicable (empty if not)
//!
template <typename t_args, typename t_value = std::string_view>
std::pair<std::optional<t_args>, std::optional<t_value>> parse_args(
    const std::vector<t_value>& args)
{
  t_args parsed_args{};
  size_t min_args_size{}, index{};

  static_assert(
      std::is_same_v<
          t_args,
          ServerArgs<t_value>> || std::is_same_v<t_args, ClientArgs<t_value>>);

  if constexpr (std::is_same_v<t_args, ServerArgs<t_value>>)
    {
      min_args_size = 2;
    }
  else if (std::is_same_v<t_args, ClientArgs<t_value>>)
    {
      min_args_size = 3;
    }

  auto const parse_error = [&](const type::kError error) {
    return std::make_pair(
        std::nullopt, args_error<t_args, t_value>(error).second);
  };

  if (args.size() < min_args_size)
    return parse_error(type::kError::Count);

  if constexpr (std::is_same_v<t_args, ClientArgs<t_value>>)
    {
      if (!parsed_args.template get_key<type::kLocation>(args.at(index)))
        return parse_error(type::kError::Location);
      parsed_args.location = args.at(index);
      index++;
    }

  if (!parsed_args.template get_key<type::kCommand>(args.at(index)))
    return parse_error(type::kError::Command);
  parsed_args.command = args.at(index);

  if (!parsed_args.template get_key<type::kSystem>(args.at(++index)))
    return parse_error(type::kError::System);
  parsed_args.system = args.at(index);

  if (args.size() >= min_args_size)
    {
      for (size_t arg{min_args_size}; arg < args.size(); arg++)
        parsed_args.system_args.push_back(args.at(arg));
    }

  return {parsed_args, std::nullopt};
}
}  // namespace sekreta::api::impl_helper

#endif  // SRC_IMPL_HELPER_HH_
