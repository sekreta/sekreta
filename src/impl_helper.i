//! \file
//!
//! \brief SWIG interface file to Sekreta's super-project implementation helper
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details

%module pysek_impl_helper
%{ #include "impl_helper.hh" %}

%include "impl_helper.hh"

//! \todo assignment operator overloading (see build warnings)
