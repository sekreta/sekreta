# Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md.
# Licensed under the BSD 3-Clause license. See LICENSE.md.

cmake_minimum_required(VERSION 3.10.2...3.16.4)

# TODO(anonimal): consider separate Sekretari library (will still rely on Sekreta interfaces)

list(APPEND SEKRETARI_HEADER_FILES
  4se.hh
  did.hh
  eds.hh
  sek.hh
  ssd.hh)

## SWIG

if (BUILD_SWIG)
  # TODO(unassigned): foreach isn't very bright with actual lists?...
  set(4se "4se")
  set(did "did")
  set(eds "eds")
  set(sek "sek")
  set(ssd "ssd")
  foreach(name IN LISTS
      4se
      did
      eds
      sek
      ssd)
    BuildSWIG(${name})
  endforeach()
endif()

## Install

install(FILES ${SEKRETARI_HEADER_FILES}
  DESTINATION include/sekreta/sekretari
  COMPONENT sekreta)
