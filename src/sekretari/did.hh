//! \file sekretari/did.hh
//!
//! \brief Implements the Sekretari DID component
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details
//!
//! \todo Merge WIP branch when in working condition

#ifndef SRC_SEKRETARI_DID_HH_
#define SRC_SEKRETARI_DID_HH_

#endif  // SRC_SEKRETARI_DID_HH_
