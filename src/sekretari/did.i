//! \file
//!
//! \brief SWIG interface file to Sekretari's DID component
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details
//!
//! \todo Make it so.

%module pysek_sekretari_did
%{ #include "sekretari/did.hh" %}
