//! \file
//!
//! \brief SWIG interface file to Sekreta's underlying system implementation
//!
//! \author See the AUTHORS.md file or use the `git log` command on this file
//!
//! \copyright BSD 3-Clause license. See the LICENSE.md file for details
//!
//! \todo Make it so.

%module pysek_system
%{ #include "system.hh" %}
