[//]: # " Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md."
[//]: # " Licensed under the BSD 3-Clause license. See LICENSE.md."

[<img width="350" src="img/sekreta.png" alt="Sekreta" />](https://sekreta.org)

[![pipeline status](https://gitlab.com/sekreta/sekreta/badges/master/pipeline.svg)](https://gitlab.com/sekreta/sekreta/commits/master)
[![codecov](https://codecov.io/gl/sekreta/sekreta/branch/master/graph/badge.svg)](https://codecov.io/gl/sekreta/sekreta)
[![coverity](https://scan.coverity.com/projects/20175/badge.svg)](https://scan.coverity.com/projects/20175/)

Sekreta is the "universal" internet-anonymity API for modern anonymity networks - with optional privacy-enhancing subsystem.

---

- [Research](#research)
- [Building](#building)
  - [Dependencies](#dependencies)
    - [Developer requirements](#developer-requirements)
  - [`make-sek`](#make-sek)
    - [Examples](#examples)
    - [Build notes](#build-notes)
    - [Installation](#installation)
    - [Re-installation](#re-installation)
- [Using](#using)
- [Contact](#contact)
- [License](#license)

## Research

What exactly *is* Sekreta? Current research and documentation can be found [here](doc/README.md).

## Building

The following are requisites for building Sekreta. See [make-sek](#make-sek) below for the actual build process.

*Note: if you intend to build in-tree anonymity systems, you must fulfill their respective requisites (see their documentation).*

### Dependencies

| Dependency          | Minimum version supported    | Optional | Arch Linux   | Raspbian      | Ubuntu          | FreeBSD       | OpenBSD                | Android (termux) |
| ------------------- | ---------------------------- |:--------:| ------------ | ------------- | --------------- | ------------- | ---------------------- | ---------------- |
| Git                 | 2.21.0                       |          | `git`        | `git`         | `git`           | `git`         | `git`                  | `git`            |
| GNU/GCC             | 8.3.0                        |          | `gcc`        | `g++`         | `g++`           | `gcc`         | NOT SUPPORTED          | NOT SUPPORTED    |
| LLVM/Clang          | 7.0.1                        |          | `clang`      | `clang`       | `clang`         | `llvm`        | `llvm`                 | `clang`
| CMake               | 3.10.2                       |          | `cmake`      | `cmake`       | `cmake`         | `cmake`       | `cmake`                | `cmake`
| SWIG                | 4.0.1                        |    X     | `swig`       | BUILD IN-TREE | BUILD IN-TREE   | BUILD IN-TREE | BUILD IN-TREE          | BUILD IN-TREE    |
| Python              | 3.6.8                        |    X     | `python`     | `python3-dev` | `python3.7-dev` | `python37`    | `$ pkg_info -Q python` | `python-dev`

#### Developer requirements

| Dependency                      | Minimum version supported    | Optional | Arch Linux   | Raspbian         | Ubuntu                | FreeBSD      | OpenBSD       | Android (termux) |
| ------------------------------- | ---------------------------- |:--------:| ------------ | ---------------- | --------------------- | ------------ | ------------- | ---------------- |
| Googletest                      | 1.8.0                        |          | `gmock`      | BUILD IN-TREE    | `googletest`          | `googlemock` | BUILD IN-TREE | `googletest`     |
| Lcov (for Gcov)                 | 1.13                         |          | `lcov` (AUR) | `lcov`           | `lcov`                | `lcov`       | NOT SUPPORTED |  NOT SUPPORTED   |
| Valgrind                        | N/A                          |          | `valgrind`   | `valgrind`       | `valgrind`            | `valgrind`   | `valgrind`    | `valgrind`       |
| Doxygen                         | 1.8.13                       |          | `doxygen`    | `doxygen`        | `doxygen`             | `doxygen`    | `doxygen`     | `doxygen`        |
| Graphviz                        | 2.36.0                       |          | `graphviz`   | `graphviz`       | `graphviz`            | `graphviz`   | `graphviz`    | `graphviz`       |
| cpupower / CPU power governance | N/A                          |    X     | `cpupower`   | `linux-cpupower` | `linux-tools-generic` | `powerd`     | TODO          | N/A              |

### make-sek

For your convenience, the `make-sek` developer script is located in the `contrib` directory and can be used as your primary building interface.

To use the script, you must have `SEKRETA_DIR` (the path to your local sekreta repository) exported into your environment or simply define `SEKRETA_DIR` before running the script.

Example:

```bash
$ SEKRETA_DIR=${HOME}/sekreta ${SEKRETA_DIR}/contrib/make-sek --release
```

Or, add the following to your `.profile` or `.bashrc` or `.bash_aliases`

```bash
export SEKRETA_DIR="${HOME}/git/sekreta"
alias make-sek="${SEKRETA_DIR}/contrib/make-sek"
```

*Note: the remaining `make-sek` examples assume the above `export` and `alias`.*

#### Examples

- A static build with library-bundled anonymity system:

   ```bash
   $ make-sek --release --static --anon=kovri --install
   ```

- Dynamic debug build + build and run all tests with valgrind:

   ```bash
   $ make-sek --debug --test --valgrind
   ```

- Build and run all benchmarks:

   ```bash
   $ make-sek --release --benchmark
   ```

- Produce all Doxygen documentation:

   ```bash
   # After building, point your browser to build/doc/doxygen/html/index.html
   $ make-sek --doc
   ```

- Build SWIG interfaces + build SWIG from source (optional):

   ```bash
   $ make-sek --release --anon=kovri --swig --build-swig
   ```

- Build and run the big kahuna:

   ```bash
   $ make-sek --release --anon=kovri --doc --swig --build-swig \
              --test --build-gtest --valgrind --benchmark --coverage \
              --install
   ```

- To cleanup after a build:

   ```bash
   $ make-sek --clean
   ```

*For all available options, run `make-sek --help`*

#### Build notes

- To use alternative compilers, use the usual `CC` and `CXX` environment variables. On FreeBSD, for example:

   ```bash
   $ CC=clang90 CXX=clang++90 make-sek
   ```

- For OpenBSD (or Raspbian on rpi4), you *must* build gtest by adding `--build-gtest` after `--test`:

   ```bash
   $ make-sek --debug --test --build-gtest
   ```

- When running benchmarks, it's highly recommended to build with the release flag:

   ```bash
   $ make-sek --release --benchmark
   ```

- When building coverage, it's necessary to build tests and also highly recommended to build benchmarks:

   ```bash
   $ make-sek --debug --coverage --test --benchmark
   ```

#### Installation

You have a multitude of installation options which are based around CMake's `CMAKE_INSTALL_PREFIX` and install target.

Using `make-sek`'s `--install`, you can install configs/targets/libs/headers/anonymity-systems:

   ```bash
   # This will install to default SEKRETA_INSTALL_DIR, which should be the build directory in SEKRETA_DIR
   $ make-sek --release --install
   ```

   ```bash
   # This will install to /usr/local - requires root privileges
   $ sudo SEKRETA_DIR=${HOME}/git/sekreta make-sek --release --install /usr/local
   ```

   ```bash
   # This will install to /tmp/frequent/install
   $ SEKRETA_INSTALL_DIR=/tmp/frequent/install make-sek --release --install
   ```

#### Re-installation

For an anonymity system(s) re-install (for example, with Kovri), you have the option of a CMake `COMPONENT` install.

Assuming that you've already done one of the above `make-sek` `--install` builds, do:

   ```bash
   $ cmake -DCOMPONENT=kovri -P ${SEKRETA_DIR}/build/cmake_install.cmake
   ```

Or, to install only Sekreta again without reinstalling an anonymity installation:

   ```bash
   $ cmake -DCOMPONENT=sekreta -P ${SEKRETA_DIR}/build/cmake_install.cmake
   ```

### Using

[//]: # "TODO(anonimal): a more generic usage example"

See Sekreta in action within [Monero](https://gitlab.com/anonimal/monero/tree/sekreta).

## Contact

- [Website](https://sekreta.org)
- [Keybase](https://keybase.io/team/sekreta)
- [Slack](https://join.slack.com/t/sekreta/shared_invite/enQtOTQ4NTExMTI0MTY1LWI4MDA1YTI3MjQ2NzNmZDk1MzllMjBlZTJiYjA0ZWE3YTM1YjEwYTUxMmFmMzFhZGEwNjdlYzU3YmI1OWJhOGU)
- [freenodeIRC](https://freenode.net) ([MATRIX.ORG](https://matrix.org))
    * `#sekreta` | usage & questions
    * `#sekreta-dev` | research & development
- Email:
    * `anonimal [at] sekreta.org`
    * PGP: [`1218 6272 CD48 E253 9E2D  D29B 66A7 6ECF 9144 09F1`](https://pgp.mit.edu/pks/lookup?search=anonimal%40sekreta.org&op=index)
- Twitter:
    * [@whoisanonimal](https://twitter.com/whoisanonimal/)

## License

Sekreta is open source software licensed under the [BSD 3-Clause license](LICENSE.md). This file, and all embodied intellectual property, are licensed under this license.
