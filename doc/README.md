[//]: # " Copyright (c) 2018-2020, authors of Sekreta. See AUTHORS.md"
[//]: # "       or use the `git log` command on this file."
[//]: # " Licensed under the BSD 3-Clause license. See LICENSE.md.

[<img width="350" src="img/sekreta.png" alt="Sekreta" />](https://sekreta.org)

## What's *your* secret?...

**This is a draft document, work in progress!**

- [Introduction](#introduction)
- [The Problems with Internet Anonymity](#the-problems-with-internet-anonymity)
- [The Sekreta Solution](#the-sekreta-solution)
- [The Sekreta Plan](#the-sekreta-plan)
    - [Sekreta Design](#sekreta-design)
        - [Usage modes](#usage-modes)
        - [Cryptography](#cryptography)
        - [Architecture](#architecture)
            - [Convenience API](#convenience-api)
            - [Sekreta API](#sekreta-api)
            - [Sekretari: The Sekreta Subsystem](#sekretari-the-sekreta-subsystem)
                - [`SEK`: The Sekreta Key](#sek)
                - [`SSD`: Sauron's Session Dispatcher](#ssd)
                - [`DID`: Dissociative Identity Delegator](#did)
                - [`4SE`: Foresee](#4se)
                - [`EDS`: Encrypted Data Store](#eds)
        - [Security Modes](#security-modes)
            - [Lowest](#lowest-security)
            - [Low](#low-security)
            - [Medium](#medium-security)
            - [High](#high-security)
            - [Highest](#highest-security)
            - [Hypothetical](#hypothetical-security)
    - [Dependencies](#dependencies)
        - [Sekreta code](#sekreta-code-requirements)
        - [Sekreta developer](#sekreta-developer-requirements)
    - [Humans](#humans)
        - [The Team](#the-team)
- [Statements](#statements)
    - [Known Issues](#known-issues)
    - [Questions](#questions)
    - [Wishlist](#wishlist)
- [Code of Conduct](#code-of-conduct)
- [Glossary](#glossary)
- [FAQ](#faq)

### Introduction

Sekreta is the first-ever "universal" internet anonymity system that also *exponentially* increases your data's confidentiality during a transmission - and does so with a seamless "universal" API.

Sekreta has the potential to be *the* **standard library** for developers of internet-capable applications and *the* **standard dependency** for privacy promoting software, services, and websites. With clear, easy-to-use, API contracts, applications will no longer concern themselves with the issues that come with cross-anonymity compatibility (choice of network, how to use said network, whether to implement any forwards-compatible application cruft, etc).

Sekreta will easily give your application greater privacy than if your application used any single anonymous network on its own - and it does so with user-friendly interfaces. Simply put, simply *set-it-and-forget-it*, with Sekreta.

### The Problems with Internet Anonymity

Anonymous overlay mix-networks like [Tor](https://wwww.torproject.org/), [I2P](https://geti2p.net/), [Kovri](https://kovri.io/), [Ire](https://github.com/str4d/ire), [Katzenpost (Panoramix messaging)](https://katzenpost.mixnetworks.org/), [LokiNet](https://github.com/loki-project/loki-network), and others, all suffer from at least a handful of numerous, significant, problems:

1. In the "all-or-nothing" problem, a single system cannot guarantee the confidentiality of a message because the underlying system may be compromised at any point during that transmission. As a result, the end-user must *trust* a single system to secure (at the very least) a single message. Should that single system ever become compromised, confidentiality is not secured nor is the message able to maintain its intended anonymity

2. The interfaces for these existing networks are either vague, difficult to use, or not uniform across all systems. In effect, an application is almost always "pinned" to choosing one system over another, depending on the use-case. Individual system constraints, idioms, and unrelated APIs all prevent applications from easily and effectively "swapping out" one system for another

3. End-users are unable to *safely* use some of these experimental systems, or future experimental systems, in the wild without fear of being compromised. Experimental systems consist of either new protocols, new implementations of pre-existing protocols, or a mix of both. This well-founded fear is turning out to be a detriment to healthy development because these systems require human users to provide realistic network modeling

4. Setting up anonymity systems can be difficult, tedious, and fine-tuning their configurations may result in user-error - which in turn may negatively affect user anonymity. Popular websites almost always end up choosing Tor over another system simply because it's "easier" to use

5. Privacy focused software that use these networks, such as certain cryptocurrencies, are always [at risk](https://www.coindesk.com/us-homeland-security-is-interested-in-tracking-privacy-coins) of losing their privacy, and continue to become riskier everyday by using single-system anonymity solutions. Because of the problem of trust imposed by these underlying network systems, single-network anonymity, as a single-privacy solution, will begin to fade out of style for privacy-focused software; much like single-core CPUs

6. If your channel is compromised in, for example, Tor or I2P, so is your data's confidentiality. The encryption/decryption process is tied directly in with the peers that you use; as this is the basis for layered routing

7. Services and applications tend to compete by choosing one mix-net system over another. This has turned out to be counter-productive for the privacy ecosystem as a whole because of the resulting mass-centralization of one system over another

### The Sekreta Solution

As time progresses, so does the need for a truly decentralized, censorship circumventing, anonymity solution. In order to achieve true network decentralization, Sekreta operates `N` underlying anonymity networks for one or more transmissions. Internally, Sekreta is a manager, a broker, an accountant, an agent, a cryptographer, and an organizer for the anonymity networks that it operates - but does so while wearing plated armor. Externally, Sekreta is your *secret secretary*... because we all have secrets :)

1. Sekreta solves the "all-or-nothing" problem by rewriting the problem into an "all-or-almost-all" problem. By distributing data as secured split-messages via shared secret sharing, Sekreta's security-enhancing subsystem will prevent message content discovery by securing your whole transmission across `N*` anonymity systems - even if `(N*)-1` systems are compromised. In other words, if you are using 3 systems (or channels), and 2 of them are compromised, your message will *remain secured* - regardless of any cryptographical failure caused by the underlying anonymity network. By also providing PFS, Sekreta can *possibly* narrow down the "all-or-almost-all" threat margin to `N*` compromised systems

2. Sekreta unites the anonymous systems by standardizing the first, ever, universal internet anonymity API. By sitting in-between your application and the underlying anonymous systems in a seamless black-box fashion, Sekreta's convenience API will shift the technical burden of the underlying systems onto Sekreta itself and not your application

3. Sekreta is extensible and allows additional underlying systems to be developed and deployed without application concern. In other words, the convenience API won't (shouldn't) need to changed as a result of new underlying systems. Sekreta encourages anonymity development by allowing developers to run free with new mix-net protocols, systems, and implementations - all of which can then be put to the test, in the wild, without end-users fearing for their data's confidentiality. Essentially, Sekreta allows developers to build anonymous systems that *they* trust while using systems that are trust-less

4. Sekreta's datastore and convenience API will both allow for easy configuration and execution for beginning and novice users, thus minimizing the potential for user-error. Simple, sweet, and easy to use. Sekreta levels the playing field for applications that are extremely Tor heavy simply because Tor provides a SOCKS proxy and somewhat easy configuration file

5. The problems inherent with single-system/single-channel solutions are neutralized and made 'a thing of the past' - thanks to the [Sekreta Plan](#the-sekreta-plan).

6. See point 5

7. See point 6

Additionally, "hidden service" destinations have, historically, been treated more like IPv4/IPv6 addresses in that "a host has an address, so that's the only address that you use". This is a false assumption, and Sekreta proves this by utilizing the underlying systems' full potential for an astronomically large set of destination identities - and merges them into into a single point.

### The Sekreta Plan

Sekreta will be both a set of libraries and optional socket-based application server - both with a "universal" API at the library and socket level.

Sekreta will shift the burden of mix-net responsibility away from the user by running the underlying anonymity networks on the user's behalf. Sekreta's APIs will give the user maximum control and flexibility - or absolute simplicity. Internally, Sekreta will interface with C/C++/Java/Rust/Go anonymity systems via their respective APIs. Ideally, these systems have C-hooks for integration. For non-C/C++ systems, they would create C-hooks via JNI, cgo, cargo, etc. respectively. Lastly, all underlying systems should provide a client/server model API (at the moment, some do, some do not).

Sekreta is designed in such a way that, if Eve passively injects or retracts any message within any underlying system, the original message sent between Alice and Bob cannot be reconstructed by either Alice or Bob. Alice and Bob can then assume that the channel is compromised or corrupt, and then setup a new channel for their session.

Sekreta should *enhance* the underlying system, not replace the system nor clobber the system's PFS. With that said, since the demand for another PFS layer may be too high, I've included a PFS feature in the ["Highest"](#highest-security) security mode. Sekreta will be flexible, allowing a client to mix-and-match the various security modes of [SSD](#ssd) and [4SE](#4se).

Sekreta will not only have a local [encrypted data store](#eds) for running local instances, but will also be able to distribute across many remote anonymity systems instances. For example, a farm of Tor servers on a separate network away from a farm of Kovri servers. Clients that would connect to the single Sekreta instance would then be able to distribute across both farms without ever having direct access (or socket access for that matter) to any farms outside of Sekreta's reach.

For details on further design and architecture, see [Sekreta Design](#sekreta-design).

Notes:

- Alice and Bob must *both* be using Sekreta if either are to take advantage of the advanced features described in the [Sekretari subsystem](#sekretari-the-sekreta-subsystem)
- Sekreta won't substantially increase overhead or latency because the underlying anonymity networks are already considered "high" latency / "high" resource systems
- Although library hooks will be ideal, in the meantime, for Java I2P, we could also easily use their I2CP API (similar to what [Vuze does](https://plugins.vuze.com/details/azneti2phelper)) and, for Tor, they have their [control protocol](https://gitweb.torproject.org/torspec.git/tree/control-spec.txt)
- "Universal" anonymity also allows for TCP and UDP transmission to be seamlessly used with a single host (thanks to [SEK](#sek)) - so long as *one* of the underlying systems supports one of the protocols, and the remaining systems support the remaining protocol

#### Sekreta Design

Before we discuss architectural details, let's consider the end-user modes of operation as well as the cryptographical algorithms to be used (as they both define the design).

##### Usage modes

Users of Sekreta will then have three modes of operation to choose from:

1. Bundled mode: interfaces are direct library function calls, either statically or dynamically; ideal for local, everyday-user, single-use applications. The underlying networks may be controlled by socket or may also be bundled in library-form

2. Network mode: uses TCP/UDP/UNIX/IPC sockets for sessions; ideal for server farms, CDNs, decentralized exchanges, web wallet backends, complex internal networks, applications that don't want to bundle, etc. (Sekreta should be able to support thousands of clients at once)

3. Transparent mode: requires network mode, but provides HTTP/SOCKS proxies with very little features or flexibility because of the limitations of those protocols. Controlling Sekreta would require a side-channel or CLI/config when Sekreta starts up

All modes will also allow forwarding of traffic to socket based applications.

##### Cryptography

**Under consideration:**

For [SEK](#sek) / [SSD](#ssd) / application layer:

- Curve/X/Ed25519 as implemented by ZeroMQ (CurveZMQ/libsodium/TweetNaCl)
    * For use as a PFS wrapper/TLS replacement at the [application layer](#the-sekreta-plan) and in the ["Highest" security](#highest-security) mode

For [4SE](#4se):

- Spicy Vegan ChaCha (20 rounds, though support for 8 and 12 rounds may be added)
- Poly1305
- SSSS (or a trivial secret sharing scheme)
- BLAKE2
- CRC-32

For [EDS](#eds):

- Argon2 (w/BLAKE2)
- ChaCha20-Poly1305

Notes:

- Whether or not Sekreta uses IETF ChaCha20-Poly1305 is still under consideration
- The use of XChaCha20-Poly1305 will almost certainly be discouraged because of [4SE](#4se)
- Argon2 and BLAKE2 variants are still under consideration

Library notes:

At this time of writing, when deciding on schemes and the choice between [Crypto++](https://github.com/weidai11/cryptopp), [libsodium](https://github.com/jedisct1/libsodium), and [Botan](https://github.com/randombit/botan), the following must be considered:

| Scheme            | Crypto++ | Botan | libsodium |
|:------------------|:--------:|:-----:|:---------:|
| Argon2            | NO       | NO    | YES       |
| PBKDF2            | YES      | YES   | YES       |
| BLAKE2            | YES      | YES   | YES       |
| ChaCha8/12        | YES      | NO    | NO        |
| ChaCha20/Salsa20  | YES      | YES   | YES       |
| Poly1305-ChaCha20 | YES      | YES   | YES       |
| Poly1305-AES      | YES      | NO    | NO        |
| Curve/X/Ed25519   | YES      | YES   | YES       |
| SSSS              | YES      | NO    | NO        |
| CRC-32            | YES      | YES   | NO        |
| X.509 ASN.1 PKCS  | YES      | YES   | NO        |

##### Architecture

There will be 4 major areas of development calling on at least 4 different areas of specialty:

1. The convenience API + Sekreta API

2. Sekretari: the Sekreta subsystem
    * `SEK`: The Sekreta Key
    * `SSD`: (Eye of) Sauron Session Dispatcher
    * `DID`: Dissociative Identity Delegator
    * `4SE`: SSSS-style `(k, n)` message fragmenting (pronounced "foresee")
    * `EDS`: Encrypted Data Store (offline)

3. Cryptographical assurances

4. The hooks into the anonymity systems / integration

Graphically, they can be described as:

       .-----------------------,
       |    Convenience API    |
       |-----------------------|
       |      Sekreta API      |
       |-----------------------|
       |       Sekretari       |
       | SEK | SSD < DID < 4SE |
       |-----------------------|
       |    Anonymity Cloud    |
       `-----------------------'

Notes:

- The Convenience API must be backwards and forwards compatible as well as completely decoupled from Sekretari
- The Sekreta API and Sekretari subsystem have more wiggle room in terms of backwards compatibility
- All components should be as modular as possible; when we need different crypto/hashing functions, the interfaces should be abstracted enough for easy swap-out

TODO(anonimal): finish this section

##### Convenience API

Sekreta's convenience API is meant to be as least-scary as possible; consisting of generalized nomenclature of user-friendly, network-agnostic terminology. If an end-user needs more security at the cost of performance, they will simply issue calls that are specific to greater "security" (via more hops or increased cryptographical overhead, for example). The details will be black-boxed by the [Sekreta API](#sekreta-api) though there will be opaque hooks into that API.

- For the terminology of the varying [security modes](#security-modes), the convenience API could include such terms as:

    * FastButWeak
    * Normal
    * SlowButStrong
    * SlowestButStrongest

       *and/or*

    * Silver(Shield/Sled)
    * Platinum(Pipe/Path)
    * Rhodium(Rod/Road)
    * Titanium(Tank/Tunnel)

- The convenience API may also have a base 6 *Finite Security Slider* where 0 = weakest and 6 = strongest
- The convenience API *should* be swappable with any other convenience API implementation

TODO(anonimal): finish

##### Sekreta API

The Sekreta API will be the most technical interface available. With this API, you have finite control over the Sekretari subsystem, which in turn has finite control over the underlying systems (dependent upon *their* APIs).

For example, you can determine which portions of your message you want to fragment so as not to reveal "sensitive" chunks across any single channel. This is extremely useful for cryptocurrency plaintext RPC transactions in which the recipient(s) and amount can be within the same session or message. With this type of fine-grained *fragment* control, you wouldn't need the overhead of encryption in the "High" and "Highest" [security modes](#security-modes). Algorithm control, crypto control, how channels are established, with whom, when, and more can all be controlled via the Sekreta API.

TODO(anonimal): they'd love to hear *all* the gory details...

##### Sekretari: the Sekreta Subsystem

The Sekretari subsystem consists of 4 major components:

1. [SEK](#sek) will allow you and your host to become multiple identities at the same time
2. [SSD](#ssd) will manage all the messages and the channels to all identities
3. [DID](#did) will decide which message fragments are mixed-in with which identity
4. [4SE](#4se) will securely fragment your message, guaranteeing it's confidentiality

###### SEK

So, how do we communicate our anonymous destinations? To do that, you'll need SEK; the Sekreta "key". This "key" is essentially a "destination bundle" or, simply put, a Sekreta "address". This key is only required for the higher [security modes](#security-modes) but may also prove useful at the application layer for "Network" and "Transparent" mode (a variant of this key will be needed for those modes regardless).

The Sekreta key structure is a binary amalgamation of all anonymity destination identities with the destination signed by its respective identity, and the CurveZMQ long-term pubkey to be used for the ["Highest"](#highest-security) mode. Every destination is signed in order to prevent a bad actor from signing destinations that they do not control, thus preventing tampering/additions/deletions of destinations when sharing a Sekreta key out-of-band.

A Sekreta key's address portion will consist of a simple list of `k` addresses per `N` systems, where chosen address(es) `a` are no greater than available `k`. For the most common usage example, we'll look at a key consisting of `(1:1)^N` pairings:

- A single onion address `(1:1)`
- Or an onion address + loki address `(1:1)^2`
- Or an onion address + loki address + i2p address `(1:1)^3`
- Or any number of single-pair bundled addresses to `N` systems `(1:1)^N`

For a use-case of different ratios, and for details on how this applies to channel/address ratios, see [DID](#did).

The Sekreta key structure, as a whole, also has the potential to be signed out-of-band, accompanied by an X.509 (or PGP) certificate, and keys could also, theoretically, be signed by a CA; but this would be more useful for clearnet websites who wish to serve mix-net. The use of both options are TBD.

The Sekreta key is also the structure of "seed addresses" for "identity bouncing" as described in [SSD](#ssd). The format specification of Sekreta key is TBD but may certainly include additional fields like expiration date/time and optional data.

Onion addresses, garlic addresses, katzenpost addresses, and any future overlay network address will (should) eventually be replaced by a Sekreta key. This is one the great unifying factors of Sekreta and the anonymity ecosystem.

An example method of distributing the Sekreta key, with some finagling, would be to use Kovri's or, any I2P implementation's network database to make room for a Sekreta key entry (this would change the spec so, if I2P or the other implementations don't play ball, they will miss out on the opportunity). When Sekreta bootstraps Kovri at startup, we simply lookup the encoded hash for the key and fetch the key from the NetDb. This hash is incredibly easy to share and, for example, can be placed on websites (like the Pirate Bay, or WikiLeaks) as a "Sekreta address". No Sekreta key file would need to be shared out-of-band by the end-user. This may be the easiest and most effective way to "publicly" share and distribute a Sekreta key. A common use-case would be web browser. For example, a user would then simply enter `http://ecodedhash.sek` and Sekreta would handle the rest (a Sekreta Web Browser or Add-on would handle this perfectly, but so would any Sekreta proxy functionality).

Sekreta key could come with a sekreta "address" that you connect to through the API. The key could have a prepended certificate bundled that would verify the remaining key (or have cert side channel or have a datastore similar to X.509 ca-certificates with associated sekreta key). The Sekreta key would have a possible expiration date too.

Notes:

- The threshold for number of identity destinations should be "reasonable". A limit may or may not be enforced in the future. TBD
- You don't need a Sekreta key to have a single-network session or a single-network session in any security mode below "High"
- You don't need a long-term CurveZMQ pubkey within a Sekreta key if the endpoint never intends on using "High" or higher security, though the pubkey may be enforced as a requisite regardless
- The addresses available in a Sekreta key should be destinations generated by Sekreta internally (via the underlying systems) and should be unique addresses for Sekreta use only. Meaning, don't use existing destinations unless they are generated by Sekreta
- Sekreta keys can be produced at startup or by a utility binary, though that is merely an implementation concern

TODO(anonimal): add optional side channel input for private party

###### SSD

TODO(anonimal): re-write this entire section into a clear spec (it's a bit messy), and finish the details

*(Eye of) Sauron Session Dispatcher* manages both the application layer sessions and underlying anonymity sessions - but with an all-seeing Eye. SSD communicates externally with the APIs, the application layer, and, internally, with the remaining Sekretari subsystem. SSD ensures that the appropriate number of message shares are collected and subsequently sent to [DID](#did) for processing.

SSD maintains the identities used for communication as defined by [SEK](#sek), which ultimately decides the threshold of shares to be processed by [DID](#did) and [4SE](#4se). SSD also generates the ChaCha20 encryption key to be used in the various [security modes](#security-modes), as well as nonces for all [security modes](#security-modes).

Other primary functions of SSD include:

- Real-time management of all underlying channels after determining how many channels are created (per single or multiple destinations)
- Allowing you to have numerous sessions with numerous hosts/identities with numerous applications. With SSD, you can have multiple applications use data from any number of destinations.
- When determining nonce, whether IV or not: by nature of the underlying anonymity network, each session is confined to its unique underlying session. This means that we can technically use the same nonce across any Sekreta sessions without the endpoint becoming confused about which data is from where, though we we'll try to avoid this out of good habit
- Forwards traffic from the underlying anonymity networks to your local application, webserver, etc.
- Manages ChaCha20 "re-key" functionality every N minutes, or every N messages (absolutely not maintaining the nonce counter across restarts or across any underlying systems). Duration of the secret key for a session and [4SE](#4se) is TBD.
- Allows you to decide your session strategy (whether to forgo any fragmenting and use a single system on a per-transmission basis, etc. TODO(anonimal): details to come
- "Destination rotation": sessions can be sent to different destinations at random intervals during a session while providing a seamless end-user experience. As an added benefit: when you switch over to a new channel for the same destinations, the new paths/channels won't have your shared secret even if all channels are compromised because the secret was already shared in previous paths (assuming that all previous paths weren't compromised or are colluding). This risk can be mitigated even further with a restricted route option. TBD.
- Operates with the various [usage modes](#usage-modes) that, like nearly everything within the Sekretari subsystem, can be configured via the APIs
- Allowing you to use a single destination address from [SEK](#sek) in any security mode from "Low" to "Highest" by creating `N` channels to that single address and [4SE](#4se)'ing the message across those channels
- "Identity bouncing": Because [SEK](#sek) encodes a bundle of destinations, they can also act as "seed addresses" so, once connected, you can periodically receive a list of *new* destinations that are kept private/unpublished but with certainty that the host you are speaking with is the one who controls those destinations (as the new destinations are signed by the old). This prevents traffic from using a single set of addresses at all times (until the [SEK](#sek) key's expiration date).
- Manages whether to mix-match clearnet traffic with hidden service destinations or entirely one or the other

Notes:

- All shares of a message must not go thru a single channel on a single system (it's pointless to do that anyway)

- If less sensitive data needs higher throughput, it can be directed through a single higher-throughput system, for example; Tor. Subsequently, more sensitive data with less-throughput requirements can, for example, be sent through Kovri or any other I2P implementation; or vice versa
- The sending of fragments *may* be done at pseudorandom intervals within a range of milliseconds to prevent correlation though this may be largely ineffective depending on the underlying system. The underlying system itself should have a proper mix distribution implemented, otherwise, *we'll literally be wasting time*
- Whether the shares of the secret ChaCha20 key are sent through the session *before* sending encrypted payloads (as a means to "initiate" a session) is TBD
- New ChaCha20 keys are generated upon restart and across new sessions so nonce reuse across *restarts* should not be a concern. However, nonce reuse during a session is, so SSD must ensure that repeated fragments are identical if they contain the same nonce after a resend, and simply drop the complete message of `k` fragments if a nonce is reused (this behavior would indicate something very broken anyway and would be dropped regardless)
- In [highest security](#highest-security) mode, each channel receives its own CurveZMQ session so, if one channel's curve is compromised, the remaining (theoretically) aren't
- Since SSD manages and operates what the underlying anonymity network reports in terms of channel health, SSD can also determine in real-time which channels are the most efficient in terms of latency and throughput and adjust accordingly. This Sekreta feature is extremely valuable for creating as much of a smooth, seamless experience for applications as possible. TODO(anonimal): handle bad sessions if shares are rejected (or any bad messages for that matter)
- The underlying systems will determine the "come back" destinations, or what the endpoint knows you as. This shouldn't be a concern of Sekreta as this is an underlying system concern
- For "Transparent mode", the application-layer proxies will autodetect the address and use the appropriate underlying system for whatever the address requires

TODO(anonimal): write application layer session spec for "Transparent" and "Network" mode. Note: applications that support ZeroMQ will simply use its underlying CurveZMQ

###### DID

The *Dissociative Identity Delegator* works with [SSD](#ssd) and [4SE](#4se) to:

- Verify the identities within [SEK](#sek)
- Ensure that fragments during a session are bound to one or more of their respective identities and their session ID
- Relay the secret ChaCha20 encryption key + nonce for [4SE](#4se) after they have been generated by [SSD](#ssd) or parsed by [4SE](#4se)
- Tell [SSD](#ssd): "send these shares through these identities", and SSD handles the rest

DID essentially decides which fragments to send to which destination after ensuring that the destinations are "legitimate".

As noted in [SEK](#sek), the *channels* used within `N` systems of `k` total addresses can be described as `c` of `k` for `N` systems or, `W=(c:k)^N` where the resulting "security weight" would prove the "weight" needed for complete compromise (where `W` must equal 0). Depending on how the nature of the underlying system's hidden service usage is implemented, `k` may or may not give weight to security (depending on how descriptors or metadata are fetched within the system) but, assuming that `k` is applicable, we will want a healthy balance of available addresses and channels. TODO(anonimal): plot and explain in detail + further research needed.

An ideal DID protocol would also include, for example:

1. A [SEK](#sek) key that would have 5 garlic destinations, 3 onion destinations, and 2 Katzenpost destinations
2. A client would then have the option of using all destinations or, for example, setting up a `3:5, 2:3, 1:2` session:
    * 3 of the 5 garlic destinations
    * 2 of the 3 onion destinations
    * 1 of the 2 katzenpost destinations
3. Even though SSD has processed all [SEK](#sek) entries, DID can, randomly, or at the will of the API, decide which of those destinations to bind fragments to, and, report back to SSD (which then send the transmission)

Another example:

1. A [SEK](#sek) key would instead provide only 3 onion destinations
2. DID will setup fragments for every circuit so, if one circuit is compromised ; the entire transmission remains confidential, even if the Tor network partially breaks your anonymity (assuming the guard node isn't compromised and all circuits aren't using that compromised guard)

TODO(anonimal): finish details and write-up a clear spec

###### 4SE

Pronounced *foresee"*, 4SE implements SSSS-style message fragmenting. 4SE takes raw data from [DID](#did) and returns `k` encrypted shares of requisite material back to [DID](#did). **IMPORTANT: for our (k, n) usage, `k` must always equal `n`!**

- All shares, regardless of security level, contain a nonce generated by an incremental counter. This nonce will be used as an identifier for ordering and for ChaCha20
- **All shares must be received in order for an endpoint to decipher the fragments into a message as a whole**

The process of 4SE depends on the type of [security mode](#security-modes) in play. The following represents [Low security](#low-security) and [Medium security](#medium-security) modes, but, for this example, we will use CRC-32 instead of BLAKE2. The process will be the same for both modes, so replace `C` with `H` as needed. The message presented may either be plaintext or ciphertext depending on the application in use and the message's state is that of *before* sending on the wire across any underlying anonymity network. Note the ASCII representations of binary data and *please excuse the psuedo math/code* as this may upset some mathematicians.

1. Generate a checksum `CM` of the message using `C`

       CM = C(M)

2. Fragment the message into `i` fragments `F` for `k` destinations (as determined by the session)

       M = F_1,...,F_k

3. The public nonce `N` generated by SSD at the beginning of the session (or incremented during) is prepended to the fragment payload `P`. Whether 64bit or 96 (IETF) is TBD (for unencrypted sessions, we may want to include a smaller nonce size)

             .-----,
             | N_i |
       F_i = |-----|
             | P_i |
             `-----'

4. A fragment checksum `CF_i` is now generated on a per-fragment basis (nonce + fragment payload)

       CF_i = C(F_i)

5. Generate `i` shares of `S` with `S` being the message checksum combined with the checksums of the fragments. For example,

       S = CM + (CF_1 + ... + CF_k) = B6BD307F6EC83E057F7E690456BD7A48

       S_1 = a80f584aeed965f8191a48ed095580afc10db0e357aeb3f342e7c369cbbb73ab
       S_2 = df49c7fd2905c01123f3a0dafb172b1ae83efdc9f88222e50c0d783c8d8dedbb
       S_3 = ac405f75f2422881ba32236570a02ced04c649c407989b55d8030fc1a2ca1e61

Notes:

- Why do we do this step? Because, once we reassemble and compare the checksum of the received message against our shared secret, if the checksum does not match, there's no easy way to determine where the *payload* corruption occurred or which compromised channel had tampered with the *payload*. So, in order to better isolate the problem, we generate a checksum of each fragment before it goes onto the wire.

6. Append a share to the nonce within each fragment

             .-----------,
             | N_i + S_i |
       F_i = |-----------|
             |    P_i    |
             `-----------'

Note: we expect the underlying anonymity network to address any bandwidth correlation issues because they themselves can fragment or pad their encapsulating messages as needed. We shouldn't need to randomize the size of the shares across all of our fragments.

7. Send `F_i` across `k` channels to your destinations and *poof*, your endpoint should:
    * Receive your fragments
    * Calculate `n` of `(k, n)` based on the nonce set in the session (and any subsequent received fragments)
        - The endpoint will only need to check the counter to reassemble `(k, n)`
    * Combine the shares and, if successful;
        - Reassemble the fragments in counter order
        - Parse out the payloads and perform `C(M)`
        - Compare the output against reconstructed `S`
        - If `C(M)` passes, the remaining checks will be skipped for the sake of efficiency. Otherwise, performs checksum comparisons based on the remaining checksums given in `S`
        - Notify DID of success, which notifies SSD, who will then increment the nonce counter
    * If shares cannot combine (missing share/fragment after specified duration or corrupt/tampered share/fragment):
        - Notify DID, which notifies SSD, who will then respond appropriately (we must never reuse the nonce for a new message with same key)

Given the above example, once reassembled, you should see the complete message as the string "message" with `F_1="mes"`, `F_2="sa"`, `F_3="ge"`.

----

For this example of [High security](#high-security) and [Highest security](#highest-security), a single message is split and then encrypted, thus treating each split as a new encrypted message, and every share includes a complete authentication tag and nonce for its respective split.

1. Fragment the message into `i` fragments `F` for `k` destinations (as determined by the session)

       M = F_1,...,F_k

2. Assuming ChaCha20-Poly1305, encrypt and generate tag for fragment `F` by using `E` (the IV is the public nonce `N` generated by SSD at the beginning of the session, or incremented during)

       EF_i = E(F_i)

3. Using the additional data field `AD`, we prepend `N` for easy fragment reassembly (as noted in the previous security modes) and decryption by the endpoint. Note: nonce size will be determined by the final decision on ChaCha20

              .------------,
              | AD_i = N_i |
       EF_i = |------------|
              |    EF_i    |
              `------------'

4. Generate `i` shares of `S` with `S` being the secret key to decrypt `EM_i`, and append `S_i` to `N_i`

              .------------------,
              | AD_i = N_i + S_i |
       EF_i = |------------------|
              |       EF_i       |
              `------------------'

5. Send `EF_i` across `k` channels to your destinations and *poof*, your endpoint should:
    * Receive and reorder your fragments in counter order
    * Calculate `n` from `(k, n)` based on the nonce set in the session (and any subsequent received fragments)
        - The endpoint will only need to check the counter to reassemble `(k, n)`
    * Combine the shares and, if successful;
        - Reconstruct `S` to create the cipher key decryption
        - Decrypt the combined fragments to expose `M`
        - Send the key to DID (which sends to SSD) for subsequent session encryption
        - Notify DID of success, which notifies SSD, who will then increment the nonce counter
    * If shares cannot combine (missing share/fragment after specified duration or corrupt share/fragment):
        - Notify DID, which will subsequently notify SSD, which will then respond appropriately (we must never reuse the nonce for a new message with same key)

Notes:

- For all security modes, payload sizes `P_i` can be randomized but must always have the minimum requirement of nonce + share size "header" for a complete fragment `F_i` ("header" sizes are determined by their respective schemes in use).
- If a share is present within the additional data field, it is assumed to be a share of the ChaCha20 secret key. Note: this may only happen once, at the beginning of a session. TBD.

###### EDS

Sekreta can optionally keep your *entire anonymity system* in an Encrypted Data Store for safe offline storage. See [cryptography](#cryptography).

All underlying systems, and their respective files for runtime and configuration, as well as Sekreta files, have the option to be encrypted and decrypted only by Sekreta using a password-based mechanism. Underlying system configuration can be done via the Sekreta API after Sekreta is started but before the underlying instances are started. This is especially ideal for devices that don't have full disk encryption by default such as [raspbian](https://www.raspberrypi.org/downloads/raspbian/) (when turning small devices into an an "anonymity box"). This also allows users to not have to fuss with other types of disk or container encryption.

#### Security Modes

Sekreta security modes describe the security of both the socket layer and the data sent over those sockets - though both components can have interchangeable security modes.

In the "Lowest" security mode, a client will have the **highest trust** in the underlying anonymity network(s). In the "Highest" security mode, a client will have the **lowest trust** in the underlying system(s).

TODO(anonimal): clarify the modes, finish the details, solidify the spec

##### Lowest Security

"Lowest" security provides an application transparent access to the underlying anonymity networks, bypassing the APIs and Sekretari completely

An example usecase for "Lowest" security mode is, for example; if you have a dedicated Tor instance which serves an internal network, and wish to use the Tor instance remotely, but you don't fully trust this internal network. This mode provides more convenience than having to setup stunnel/ssh forwarding for every client on your internal network but every client application has the capability of using a Tor SOCKS proxy (or [TransProxy](#wishlist) for that matter.

"Lowest" security can also mean using a single underlying system for all your traffic, even though the traffic is fragmented. For example, using only Tor but setting up multiple circuits for a single session, and sending your fragments across all circuits, as described in [DID](#did).

##### Low Security

This implements [medium security](#medium-security) but with CRC-32 instead of BLAKE2 for [4SE](#4se).

TODO(anonimal): finish

##### Medium Security

"Medium" security has at least two definitions:

- As described in [4SE](#4se)
- An application has transparent access to the underlying anonymity networks as described in [lowest security](#lowest-security) but with an additional application layer of CurveZMQ applied for Network mode security

TODO(anonimal): finish

##### High Security

"High" security is most applicable to [4SE](#4se) but also describes the application levels socket security in ["Medium"](#medium-security) mode.

TODO(anonimal): finish

##### Highest security

In addition to the encrypted fragments in [4SE](#4se), "Highest" security mode implements CurveZMQ across the channels used in an [SSD](#ssd)/[SEK](#sek) session. [SSD](#ssd) will wrap each channel that [4SE](#4se) fragments are sent on with X25519 PFS. If a channel(s) is compromised, Eve cannot even see fragments (shares) of the shared key without breaking the PFS of the channel.

##### Hypothetical Security

Extensibly, this would be a hypothetical layer of [supersingular elliptic-curve isogeny cryptography](https://en.wikipedia.org/wiki/Supersingular_isogeny_key_exchange), acting as a forwards-compatible post-quantum replacement to our use of X25519.

This mode of security should only be needed if the underlying anonymity networks do not implement a PQ model, and this mode will most likely never be needed if the underlying cryptosystems are PQ secured.

#### Dependencies

Dependencies are determined by which underlying systems you wish to use. More systems will create more dependencies. At the very least, you'll need one underlying anonymity network.

For example, if you wish to use only Tor and Kovri, and are using the "Bundled" option, you won't need to install any new dependencies (assuming that Tor is lib-ready, like Kovri). For all other underlying systems and options, Sekreta will aim to make the dependency pull-in process as painless as possible.

If the anonymity systems that use Java, Rust, and Go, provides C bindings, or even designated APIs for "Network" mode, you would need to install their respective environments. This is an unavoidable side-effect of using any software that uses kitchen-sink environments (like Java, Rust, and Go).

##### Sekreta Code Requirements

- C++17
- Crypto library TBD (see [cryptography](#cryptography))
- ZeroMQ

Notes:

- I would like to avoid Boost if possible
- API components of Sekreta (like the hooks) may certainly be in C. TBD.

##### Sekreta Developer Requirements

Though these are a given, here are additional requirements for development only:

- a sane compiler
- googletest
- valgrind
- Doxygen
- Docker
- SWIG

### Humans

#### The Team

As of time of writing, yours truly, but, as an opensource project, a team will always be a work in progress.

As for teamwork, the work could be divided into at least the following sections:

- Convenience API
- Sekreta API
- Sekretari subsystem
- Cryptographical assurances
- Hooks into the underlying anonymity networks

I'm currently working on all sections though the more hands the merrier.

### Statements

**Any of my associations, past, present, or future, will not make me biased against any underlying systems so long any system in questions is not knowingly malicious. Sekreta does not endorse any single anonymity network over another. I wish to bring all systems together for the greater good of all anons everywhere.**

- Sekreta is not an overlay network, although it does require at least one
- Sekreta is not an cryptographical standard, although it does use proven cryptographical standards
- Sekreta is not produced, nor made biased, by any nation-state, government agency, or private enterprise
- Sekreta will not be backdoored and will not be made less secure as a result of any financial/personal interest or gain
- Sekreta is a good ol' fashioned library/application with everyone's privacy in mind
- Sekreta does not prioritize one application use-case over over another
- Sekreta benefits every anonymous overlay project
- Sekreta is for all anons, everywhere

*Fun tip: line up all the protocols and you'll see a poetic quip: "Sekreta's Key: Sauron's Session Dispatcher Did Foresee (an) Encrypted Data Store" (yes, an intentional part of the design)*

#### Known Issues

- Bottle-necking (this could be alleviated with a good distribution algorithm)
- Concurrency and memory usage as related to bottle-necking
- Overhead with higher security levels (well, that's physics for you)
- DoS or partition attack replacing shares? Certainly, but that would also certainly expose the channel(s) as being compromised. As a result, Sekreta will build new channels and initiate a new session. The security implications of partitioning attacks goes beyond Sekreta and into the underlying systems. If all channels are compromised before a session is created, that is out of the hands of Sekreta. This problem is a side-effect of [spacetime](https://www.youtube.com/watch?v=PRSRHakFS3g)
- There may certainly be ways to hack what's currently written. Stay tuned.

#### Questions

- How effective are the present solutions in the [Sekreta plan](#the-sekreta-plan)? For example, is the jump from CRC-32 to BLAKE2 in Sekreta's [security modes](#security-modes) an effective security improvement? If a share is compromised, a BLAKE2 hash is certainly far more difficult to compromise than a CRC-32 code.
- Security implications of the nonce counter implementation?
- For "High" [4SE](#4se), do we want an option to have a single message encrypted and then split into fragments, then the shares for each fragment include fragments of the authentication tag and the nonce that was attached to the encrypted message? This may require sending a "real" header. TBD.
- By conducting channel tests and RTT measurements, can we develop an effective algorithm for *fragment* load balancing? This includes payload-size-to-throughput ratio
- Do we want aim for FIPS compliance? If so, we'll need to reconsider some of these algorithms
- What licensing concerns can we expect when distributing the underlying systems?
- How can we increase the quality of the security provided?
- Algorithms: how can we do this better?
- What sort of metrics can we use?
- What existing specs/formats can we use?

#### Wishlist

- TransProxy support. iptables would route all traffic through Sekreta's "TransPort" and Sekreta would transparently redistribute to the appropriate underlying system.
- Fiber owner + bank database query management: the ability to determine channel creation by both the *fiber owner* and the *bank* that owns the *owner* of the fiber.
    * This feature would require an external database which, if not queried correctly, could expose how channels are created but, once that risk is mitigated, this feature should provide the ability to build channels based not only on IP location or country but rather by the proposed variables. For example, Tor's Atlas provides information on fiber owner but not direct banker information.
    * The importance of this feature is an understatement: Nation States are no longer the only ones bringing the threat of global surveillance of both public and private information to the world: whomever owns multiple fiber locations across the globe can also own access to information across those locations; legally or not. A database of said owners *may* help to mitigate that threat.
    * Note: the database for this wishlist item has yet to exist (as far as I known) but should be publicly available depending on country.

### Code of Conduct

The license requirement of Sekreta requires that a Code of Conduct **never be written or implemented** within the Sekreta codebase, project, or forks of Sekreta.

With that said, consider the following - as they tend to create repercussions in life:

- Being disrespectful or conducting off-topic personal attacks
- Not asking questions before making accusations
- Not staying focused on the task at hand
- Not providing empirical evidence
- Any spam, on-topic or off-topic

Those with power may create repercussions.

### Glossary

- `TBD`: To Be Determined
- `PFS`: Perfect Forward Secrecy
- `Fragment/Share/Split`: These terms are used interchangeably. In this document, they describe a *single* share used within in a secret sharing scheme
- `Mix/Overlay Network`: A network used on top of the internet
- `Path/Channel`: In this document, a `circuit` in Tor terminology, a `tunnel` in I2P terminology
- `Destination/Identity`: A cryptographically bound non-clearnet address within an anonymity system
- `Clearnet`: Essentially, the internet that the overlay networks live on top of. In the "clear", regardless of a crypto system used. The "not anonymous" internet

### FAQ

- Q. Who are you?
- A. [anonimal](#contact). Most internet people know me from my work with [Monero](https://getmonero.org), [Kovri](https://kovri.io), [I2P](https://geti2p.net), and [Crypto++](https://cryptopp.com)


- Q. Why are you doing this?
- A. I'm coping with the [limitations of our measurable existence](https://www.youtube.com/watch?v=PRSRHakFS3g) until Privacy Physics becomes a household name.


- Q. What applications can use Sekreta?
- A. Any internet-capable application.


- Q. Will you have a SOCKS proxy so I can just use some dumb easy anonymity?
- A. Yes'ish, but that would defeat the point of Sekreta because you'd only be able to use a single path for a destination on a single network, or be limited to clearnet through Tor. There's the possibility of a SOCKS-friendly spec which allows an app to negotiate a session with the SOCKS proxy and, in that session, utilizes a list of addresses or the Sekreta key itself before proceeding to use the SOCKS proxy for remaining data transfer. If not that or similar, your data would be limited to something akin to the SOCKS proxies implemented by the underlying systems (Tor and Kovri, for example). TBD.


- Q. Do we want verifiable secret sharing?
- A. Because of Poly1305, this wouldn't (shouldn't) be necessary for the higher [security modes](#security-modes), but we may wish to mix-in fake shares for the lower security modes. TBD.


- Q. What if all channels are compromised?
- A. Welcome to [spacetime](https://www.youtube.com/watch?v=PRSRHakFS3g).


- Q. Does Sekreta provide more anonymity *or* data confidentiality?
- A. Both terms are relative and born out of spacetime. Sekreta provides both.


- Q. Anonymity? Privacy? You said [there was no such thing](https://www.youtube.com/watch?v=vIQq1sAe0Dw)
- A. Sadly, yes, there isn't, and the words are ill-defined - but the words have also become a pillar for privacy... and communication is all about compromise... so, here we are.


- Q. What does Sekreta and Sekretari mean?
- A. Translated, "Secret" and "Secretary", in Esperanto.


- Q. How long will this take to develop?
- A. [Time and Cost](#time-and-cost)


- Q. I use Tor and I2P and all I do is host a different address for each system. I have a garlic address and an I2P address. How is Sekreta different?
- A. You're using both in parallel but not in unison. Data sent to one destination in one system never sees the path of another system. If that single path is compromised, so is your data. Sekreta uses all available paths and cryptographically ensures that even if a path is compromised, your data is still secured and not able to be reassembled nor deciphered in its entirety. Sekreta also incentivizes its use by providing extra security data confidentiality on top of any underlying anonymity network.


- Q. Where are the diagrams?! Flowcharts?! UML?! How do you define the architecture?!
- A. I would upload the photos of my whiteboard if I could - but even those are illegible to anyone else but myself. I'm getting everything out as fast as possible so, sit tight and stay tuned.


- Q. How can I contribute? Can I get paid to contribute to Sekreta?!
- A. [Contributing](#contributing)


- Q. What technical concerns are there?
- A. The questions of concurrency and system management are to be addressed. Sekreta will be threadsafe and will have clear interfaces to setup/initialize/run/and shutdown any given anonymity system. Simply put, Sekreta will run its own instances of said systems, with dedicated hidden service endpoints which can be publicly published along with a Sekreta key.


- Q. Developer concerns?
- A. Developer guides as well as necessary files (clang-format/tidy), SWIG, tools, and more, will come with time.


- Q. Why C/C++? Don't you know it's [current year]?
- A. Because the conqueror becomes the conquered (don't you forget that), and because C/C++ is the quickest I can whip up for a reference implementation. Another reason being both functional and syntactical native C interoperability of which all the underlying non-C/C++ anonymity systems will interface with. There are many other reasons too, but I'm sure they are blog-worthy.


- Q. Why not use Sphinx for a message format?
- A. TBD, but it's not out of the question - yet. I'd like to *enhance* the underlying systems more so than build another system on top of another system - on top of yet another system.


- Q. Is Sekreta threadsafe?
- A. Yes, should be.


- Q. Where's the code?
- A. Right behind you, or in the [repo](https://gitlab.com/sekreta/sekreta/)

- Q. Can you ELI5 the primary security benefits of Sekreta?
- A. On the topic of added security:

So, you know those tiny little racing car tracks? For little toy cars? I think they mostly come in small play-sets for maybe one child or two but I've also seen them take up the size of a warehouse (at least what I've seen online, "slot car racing").

Take a handful of these toy cars. Lay them all out in front of you. On each car, spell out your message with one letter for each car. Let's say "Hi, mom!" (or, more accurately, a cryptocurrency RPC transaction). But, even better, you write your message in a language that only you and Mother speak so that no one else can understand what you're saying.

Now, you have at least several options: take all of your cars and put them into the racetrack slots of one play-set (single anonymity network, multiple channels), or take all your cars and put them into the racetrack slots across *multiple* play-sets (this is the preferred method: multiple anonymity networks, multiple channels).

At the end of *all* the play-sets is Mother, ready to receive your message. Once all the cars go through the play-set(s), they all come out on the other end intact - as originally intended. Some get there sooner than others but, ultimately, all the cars should be able to finish the race. Once received, Mother will re-arrange the cars into the original message using the language that only you and her understand. When she goes to reply, she repeats this same process from the beginning.

Now, if a spy tried to grab a car off the racetrack in order to piece together the message, they would have to break through armor-plated piping to do so (very difficult, almost impossible)! These pipes surround the slots themselves. If a spy somehow broke through this armor in all but one slot, they would still only see gobbledygook because they don't understand Mother's tongue.

If a spy, again, tried to take a car off the track, but this time replaced it with another car (in order to try and fool you or Mother), Sekreta will ensure that either you or Mother (depending on who sent the cars) are notified of the forgery so the cars can be resent across another play-set or onto another set of racetrack slots.

So, why didn't you both write a single message onto a single car, and then send that car across a single racetrack slot? Because you both knew better than to trust the whole message across a single racetrack. If someone steals or replaces your car, or kicks it off the track, or spies on your car, or re-routes your racetrack; given the racetrack's current technological capability, you have little-to-no assurances that any of these behaviors didn't happen without you knowing - or that they are not happening right now at this very moment.
